#include <stdio.h>

int main() {
  //printf("%c", 97);
  int c;
  int hist[26];
  for (int i = 0; i<26; i++){
    hist[i]=0;
  }
  while ((c=getchar()) != '0'){
    hist[c-97]++;
  }
  //normalize
  int max=0;
  for (int i = 0; i<26; i++){
    if(hist[i]>max){
      max=hist[i];
    }
  }
  int val[26];
  for (int i = 0; i<26; i++){
    val[i]=(hist[i]*1.0/max)*10;
  }
  //print
  printf("\n");
  for(int i = 0; i<26;i++){
    printf("-");
  }
  for (int i = 10; i>0; i--){
    for(int h = 0; h<26; h++){
      if(val[h]>=i){
	printf("*");
      }
      else{
	printf(" ");
      }
    }
    printf("\n");
  }
  for(int i = 0; i<26;i++){
    printf("-");
  }
  printf("\n");
  for(int i = 0;i<26;i++){
    printf("%c", i+97);
  }
}
