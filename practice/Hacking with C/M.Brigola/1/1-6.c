#include <stdio.h>

int main()
{
    printf("Thing:\n");
    int c;
    while ((c = (getchar()) != EOF))
    {
        putchar(c);
        printf("%d\n", c);
    }
}