#include <stdio.h>
int main()
{
    float fahr, celsius;
    float lower, upper, step;

    lower = -17.8;
    upper = 148.9;
    step = 11.1;

    celsius = lower;
    while (celsius <= upper) {
        fahr = 9 * (celsius) / 5 + 32;
        printf("%6.1f %3.0f\n", celsius, fahr);
        celsius += step;
    }
}