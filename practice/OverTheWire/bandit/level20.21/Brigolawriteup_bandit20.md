# Bandit Level 20
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 20 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | nc                          |

## Summary

To solve this problem, I had to open up a port that also read a file and then connect to the port that I opened up.

## Problem
There is a setuid binary in the homedirectory that does the following: it makes a connection to localhost on the port you specify as a commandline argument. It then reads a line of text from the connection and compares it to the password in the previous level (bandit20). If the password is correct, it will transmit the password for the next level (bandit21).

**NOTE:** Try connecting to your own network daemon to see if it works as you think

## Solution
After logging into the level, I did 'ls' to see what files were available to me. It showed a file called 'suconnect' so I executed that file with "./suconnect". It said that I needed to specify a port number as part of executing the file. I then used netcat to open a port with "cat /etc/bandit_pass/bandit20 | nc -lpv 33333" that also would read the old password at the same time. I then had to open a new window of Windows PowerShell and connect with ""./suconnect 33333" and then the new password was printed on the first window.


## Conclusion
I had some problems opening up the port because the window kept just becoming unresponsive. With some help, I learned that my problem was that I was only using -l instead of -lpv.

