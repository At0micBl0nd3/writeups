# Bandit Level 16
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 16 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | openssl, s_client, nmap     |


## Summary
To solve this problem, I had to scan the given ports to see which were actually open and then test to see which open ports could speak SSL. The other tough part of this problem was trying to figure out how to use the received RSA Private Key to login to the next level.

## Problem
The credentials for the next level can be retrieved by submitting the password of the current level to **a port on localhost in the range 31000 to 32000**. First find out which of these ports have a server listening on them. Then find out which of those speak SSL and which don’t. There is only 1 server that will give the next credentials, the others will simply send back to you whatever you send to it.

## Solution
First I had to login to level 16 by typing the command below and then typing in the password:

ssh bandit16@bandit.labs.overthewire.org -p 2220

The first command needed to solve this level was 'nmap localhost -p31000-32000' to get a list of the ports between 31000 and 32000 that were actually open. After this, since I only got about 5 ports in return, I just tested an SSL connection on each individual port to see which one actually let me input the password and then outputted credentials. What I received back on port 31790 was actually a RSA Private Key instead of just a password. Then, I just copy and pasted this private key (including the begin and end lines) into a txt file on my desktop. 


## Conclusion
The tougher part of this problem was probably just trying to figure out how to login to level 17.

