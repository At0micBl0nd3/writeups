# Bandit Level 10
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 10 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | base64                      |


## Summary
I had to learn how to decode a base64 encryption

## Problem
The password for the next level is stored in the file **data.txt**, which contains base64 encoded data

## Solution
First I had to login to level 10 by typing the command below and then typing in the password:

ssh bandit10@bandit.labs.overthewire.org -p 2220

Since the .txt file used a base64 encoding, I did 'man base64' to find an argument to decode the file. I found that argument, and just did 'base64 -d data.txt' to get the password.


## Conclusion
This was helpful for learning how to use the base64 command to decode an encoded file.





