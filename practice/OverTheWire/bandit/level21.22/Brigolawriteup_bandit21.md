# Bandit Level 21
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 21 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | cron, crontab               |

## Summary

To solve this problem, I had to follow a trail of breadcrumbs from one file to the next to find the file with the password.

## Problem
A program is running automatically at regular intervals from **cron**, the time-based job scheduler. Look in **/etc/cron.d/** for the configuration and see what command is being executed.

## Solution
After logging into the level, I did 'ls' to see what files were available to me. It showed nothing so I just did 'cd /etc/cron.d/' to view the configuration of jobs. I saw a bandit22 file so I did 'cat /usr/bin/cronjob_bandit22.sh' and if it gave me another file to go to. I then did 'cat tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv' and that file printed out the password for the next level. 


## Conclusion
This level was not too bad and just consisted of figuring out what was important in the cron job.

