# Bandit Level 12
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 12 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | gunzip, tar, bunzip2, xxd   |


## Summary
This problem involved just decompressing the same file over and over again and having to use the 'file' command to see which type of decompression algorithm to use. It rotated between tar, gzip, and bzip2 as the compression algorithms. It also started off as a hexdump.

## Problem
The password for the next level is stored in the file **data.txt**, which is a hexdump of a file that has been repeatedly compressed. For this level it may be useful to create a directory under /tmp in which you can work using mkdir. For example: mkdir /tmp/myname123. Then copy the datafile using cp, and rename it using mv (read the manpages!)

## Solution
First I had to login to level 12 by typing the command below and then typing in the password:

ssh bandit12@bandit.labs.overthewire.org -p 2220

The first thing I had to do for this problem was do 'xxd -r data.txt' to reverse the hexdump. After that, it basically involved a ton of reading the manual pages for tar, gzip, and bzip2 to figure out how to decompress files. The next phase just involved spamming either 'tar x -f [file]', 'gunzip [file.gz]', or 'bunzip2 [file]' to decompress then immediately doing a 'file [file]' to see which command to do next. 


## Conclusion
This was helpful for learning how to use all of the different compression/decompression algorithms. However, this took a decent amount of time and was very tedious.





