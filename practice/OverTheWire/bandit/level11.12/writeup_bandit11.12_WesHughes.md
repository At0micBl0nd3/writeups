# bandit11.12
By Wesley Hughes
___


| Problem Info | Value                                                        |
| ------------ | ------------------------------------------------------------ |
| Problem      | Bandit11.12                                                  |
| CTF          | OverTheWire/Bandit                                           |
| year         | 2020                                                         |
| points       | 0                                                            |
| category     | practice                                                     |
| tags         | grep, sort, uniq, strings, base64, tr, tar, gzip, bzip2, xxd |


## Summary
Read the file to get the password then ssh'd into the next level.

## Problem
The password for the next level is stored in the file **data.txt**, where all lowercase (a-z) and uppercase (A-Z) letters have been rotated by 13 positions

## Solution

I did cat data.txt | tr 'A-Z' 'N-ZA-M' | tr 'a-z' 'n-za-m' to rotate to the letters and unveil the following password:

5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu


## Conclusion
Short problem
