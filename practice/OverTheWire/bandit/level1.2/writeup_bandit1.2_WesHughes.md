# bandit1.2
By Wesley Hughes
___


| Problem Info | Value              |
| ------------ | :----------------- |
| Problem      | Bandit1.2          |
| CTF          | OverTheWire/Bandit |
| year         | 2020               |
| points       | 0                  |
| category     | practice           |
| tags         | ls, cat, path      |


## Summary
cat the file named - to read the password to get into the next level

## Problem
The password for the next level is stored in a file called **-** located in the home directory

## Solution
I had to do cat ./- because doing cat - does not work since - is a special character in bash. This resulted in the password for the next level being displayed: CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9


## Conclusion
Short problem





