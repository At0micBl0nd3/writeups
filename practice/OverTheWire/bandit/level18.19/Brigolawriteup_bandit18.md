# Bandit Level 18
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 18 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | ssh, cat                    |


## Summary
The main trick of being able to do this level is knowing that you can attach a command to the end of an ssh command. It took me a while to realize that this was possible.

## Problem
The password for the next level is stored in a file **readme** in the home directory. Unfortunately, someone has modified **.bashrc** to log you out when you log in with SSH.

## Solution
The main problem with this level is that as soon as your machine connects to the ssh server, it is immediately booted off. The main piece of knowledge needed to solve this level is knowing that the 'ssh' command can also just take a command to execute on that server without fully logging into it. For this level, I just had to do:

ssh bandit18@bandit.labs.overthewire.org -p 2220 cat readme

This concatenated the readme file without actually logging into the bandit server and outputted the password for the next level.


## Conclusion
This problem took a while because I did not realize that commands could just be directly executed on an ssh server. The manual page was a bit annoying to read, once again.

