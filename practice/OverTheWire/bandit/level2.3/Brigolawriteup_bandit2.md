# Bandit Level 2
By Massimo Brigola
___


| Problem Info | Value                      |
| ------------ | -------------------------- |
| Problem      | OverTheWire Bandit Level 2 |
| CTF          | Bandit                     |
| year         | 2020                       |
| points       | 0                          |
| category     | training, bandit           |
| tags         | ssh, cat                   |


## Summary
The main thing I had to research to solve this level was how to concatenate a file with spaces in the filename without Windows PowerShell interpreting the filename as multiple different arguments. I found that the best way to do this is to simply put quotation marks around the file name.

## Problem
The password for the next level is stored in a file called **spaces in this filename** located in the home directory.

## Solution
First I had to login to level 2 by typing the command below and then typing in the password:

ssh bandit2@bandit.labs.overthewire.org -p 2220

I then used the command 'ls' to list the directories to see what the exact filename was. After that I just did "cat 'spaces in this filename'" and the password for level 3 was printed out.


## Conclusion
This was a helpful problem to learn how PowerShell interprets multiple arguments.





