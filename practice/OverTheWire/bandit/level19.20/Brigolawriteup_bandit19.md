# Bandit Level 19
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 19 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | setuid                      |


## Summary
To solve this problem, I had to execute the 'cat' command as another user to view the password.

## Problem
To gain access to the next level, you should use the setuid binary in the homedirectory. Execute it without arguments to find out how to use it. The password for this level can be found in the usual place (/etc/bandit_pass), after you have used the setuid binary.

## Solution
After logging into the level, I did 'ls' to see what files were available to me. It showed a file called 'bandit20-do'. Following the instructions for the level, I executed the file with './bandit20-do' and it said to execute a command as another user with an example. I did the example command of './bandit20-do id' to view the id information about the file, and it did show bandit20 as the user. So then I just had to do './bandit20-do cat /etc/bandit_pass/bandit20' to concatenate the password.


## Conclusion
Just figuring out how to execute the file took a bit of trial and error and research.

