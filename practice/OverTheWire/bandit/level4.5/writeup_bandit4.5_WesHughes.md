# bandit4.5
By Wesley Hughes
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | Bandit4.5                   |
| CTF          | OverTheWire/Bandit          |
| year         | 2020                        |
| points       | 0                           |
| category     | practice                    |
| tags         | ls, cd, cat, file, du, find |


## Summary
Read the file to get the password then ssh'd into the next level.

## Problem
The password for the next level is stored in the only human-readable file in the **inhere** directory. Tip: if your terminal is messed up, try the “reset” command.

## Solution
I had to use the command file to view which file was human-readable. I used file -./* to see all 8 or so files at once and what they contained. Once it displayed which was ASCII-text I did cat ./-file0* for the corresponding file and got the password: koReBOKuIDDepwhWk7jZC0RTdopnAYKh


## Conclusion
Short problem





