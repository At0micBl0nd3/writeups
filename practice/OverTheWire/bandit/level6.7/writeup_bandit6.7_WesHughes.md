# bandit6.7
By Wesley Hughes
___


| Problem Info | Value                             |
| ------------ | --------------------------------- |
| Problem      | Bandit6.7                         |
| CTF          | OverTheWire/Bandit                |
| year         | 2020                              |
| points       | 0                                 |
| category     | practice                          |
| tags         | ls, cd, cat, file, du, find, grep |


## Summary
Read the file to get the password then ssh'd into the next level.

## Problem
The password for the next level is stored **somewhere on the server** and has all of the following properties:

- owned by user bandit7
- owned by group bandit6
- 33 bytes in size

## Solution
I had to use the find command to specify multiple things. Since it was **somewhere** on the server, I went as far back as I could in the directory and used find from there. I entered the command find -type f -size 33c -user bandit7 -group bandit6 to specify a file that meets the requirements in the problem. I expected to get one result, but instead got bombarded with messages, "Permission denied". I learned how to use the grep command and used grep -v "Permission denied" (or, alternatively just appending 2>/dev/null) to the message. I then found the file, bandit7.password, in a seemingly random directory and got the password: HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs


## Conclusion
Short problem





