# Bandit Level 8
By Massimo Brigola
___


| Problem Info | Value                      |
| ------------ | -------------------------- |
| Problem      | OverTheWire Bandit Level 8 |
| CTF          | Bandit                     |
| year         | 2020                       |
| points       | 0                          |
| category     | training, bandit           |
| tags         | uniq, sort, piping         |


## Summary
I had to learn how to use the 'uniq' and 'sort' commands. I then had to learn how to pipe these two commands together to use them on the same output. The formatting for piping commands was a bit confusing at first and definitely took some trial and error, but I got the result eventually.

## Problem
The password for the next level is stored in the file **data.txt** and is the only line of text that occurs only once

## Solution
First I had to login to level 8 by typing the command below and then typing in the password:

ssh bandit8@bandit.labs.overthewire.org -p 2220

I then had to learn how to correctly pipe together 'sort' and 'uniq' so that the non-unique lines of text would be filtered out. I had to do this because 'uniq' only filters lines that are the same when they are next to each other. The correct line of code to do this was:

sort data.txt | uniq -c

This command gave me the only unique line in there, the password.


## Conclusion
This was helpful for learning how to pipe together commands and use them simultaneously.





