# bandit3.4
By Wesley Hughes
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | Bandit3.4                   |
| CTF          | OverTheWire/Bandit          |
| year         | 2020                        |
| points       | 0                           |
| category     | practice                    |
| tags         | ls, cd, cat, file, du, find |


## Summary
Read the file to get the password then ssh'd into the next level.

## Problem
The password for the next level is stored in a hidden file in the **inhere** directory.

## Solution
I used ls -al (you could also use find) to see the name of the hidden file, .hidden (which is hidden by the use of . in front of it). I then did cat .hidden to get the password for the next level: pIwrPrtPN36QITSp3EQaw936yaFoFgAB


## Conclusion
Short problem





