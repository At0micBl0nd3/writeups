# Bandit Level 9
By Massimo Brigola
___


| Problem Info | Value                      |
| ------------ | -------------------------- |
| Problem      | OverTheWire Bandit Level 9 |
| CTF          | Bandit                     |
| year         | 2020                       |
| points       | 0                          |
| category     | training, bandit           |
| tags         | strings                    |


## Summary
I had to learn how to use the 'strings' command for this level

## Problem
The password for the next level is stored in the file **data.txt** in one of the few human-readable strings, preceded by several ‘=’ characters.

## Solution
First I had to login to level 9 by typing the command below and then typing in the password:

ssh bandit9@bandit.labs.overthewire.org -p 2220

Since the problem statement intentionally used the word 'strings', I figured this would be a good first command to test out. So all I did was 'strings data.txt' and then it outputted a bunch of symbols along with some equal symbols and the password right afterwards.


## Conclusion
This was helpful for learning how to use the strings command.





