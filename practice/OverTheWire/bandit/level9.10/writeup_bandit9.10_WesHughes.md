# bandit9.10
By Wesley Hughes
___


| Problem Info | Value                                                        |
| ------------ | ------------------------------------------------------------ |
| Problem      | Bandit9.10                                                   |
| CTF          | OverTheWire/Bandit                                           |
| year         | 2020                                                         |
| points       | 0                                                            |
| category     | practice                                                     |
| tags         | grep, sort, uniq, strings, base64, tr, tar, gzip, bzip2, xxd |


## Summary
Read the file to get the password then ssh'd into the next level.

## Problem
The password for the next level is stored in the file **data.txt** in one of the few human-readable strings, preceded by several ‘=’ characters.

## Solution

I did 'strings data.txt | grep "=="' to get only the readable strings and passed that into grep so that I only found the strings with several '=' within them which is where I found the password: truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk


## Conclusion
Short problem
