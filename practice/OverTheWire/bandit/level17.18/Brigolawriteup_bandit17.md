# Bandit Level 17
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 17 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | diff                        |


## Summary
For this problem, I just had to use the 'diff' command to compare the two files that were given to me.

## Problem
There are 2 files in the home directory: **passwords.old and passwords.new**. The password for the next level is in **passwords.new** and is the only line that has been changed between **passwords.old and passwords.new**

## Solution
First, I had to login to level 17 by typing the command below (with an option to use a private key):

ssh bandit16@bandit.labs.overthewire.org -p 2220 -i 17key.txt

The only command I need to solve this problem was 'diff --suppress-common-lines passwords.old passwords.new' and then it outputted the only line that was not common between the two.


## Conclusion
This problem was pretty simple since 'diff' was a recommended command and the manual page was pretty simple.

