# Bandit Level 7
By Massimo Brigola
___


| Problem Info | Value                      |
| ------------ | -------------------------- |
| Problem      | OverTheWire Bandit Level 7 |
| CTF          | Bandit                     |
| year         | 2020                       |
| points       | 0                          |
| category     | training, bandit           |
| tags         | grep                       |


## Summary
I had to learn how to use the 'grep' command to search for a specific string in a .txt file.

## Problem
- The password for the next level is stored in the file **data.txt** next to the word **millionth**

## Solution
First I had to login to level 7 by typing the command below and then typing in the password:

ssh bandit7bandit.labs.overthewire.org -p 2220

The first thing I did was 'cat data.txt' just so I could see what I was working with and how the text file was formatted. After reading up on the manual page for grep, I saw that there was a pretty easy solution for me. All I had to execute was 'grep millionth data.txt' and it spit out the line with 'millionth' and the password right next to it. 


## Conclusion
This was helpful for learning how to search for specific words within a text file.





