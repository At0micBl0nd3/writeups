# bandit6.7
By Wesley Hughes
___


| Problem Info | Value                                                        |
| ------------ | ------------------------------------------------------------ |
| Problem      | Bandit6.7                                                    |
| CTF          | OverTheWire/Bandit                                           |
| year         | 2020                                                         |
| points       | 0                                                            |
| category     | practice                                                     |
| tags         | grep, sort, uniq, strings, base64, tr, tar, gzip, bzip2, xxd |


## Summary
Read the file to get the password then ssh'd into the next level.

## Problem
The password for the next level is stored in the file **data.txt** next to the word **millionth**

## Solution

I read about what the other commands do/are used for but all I needed to do for this problem was do grep "millionth" data.txt which displayed the password next to the word where it was in the txt file: cvX2JJa4CFALtqS87jk27qwqGhBM9plV


## Conclusion
Short problem





