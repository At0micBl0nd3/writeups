# Bandit Level 23
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 23 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | nano, cp                    |

## Summary

To solve this problem, I had to write my own shell script and then use the cronjob to execute it.

## Problem
A program is running automatically at regular intervals from **cron**, the time-based job scheduler. Look in **/etc/cron.d/** for the configuration and see what command is being executed.

**NOTE:** This level requires you to create your own first shell-script. This is a very big step and you should be proud of yourself when you beat this level!

**NOTE 2:** Keep in mind that your shell script is removed once executed, so you may want to keep a copy around…

## Solution
Since this problem is pretty long to solve, I will just list the commands I used to solve the level:

cd /etc/cron.d

ls

cat cronjob_bandit24

cat /usr/bin/cronjob_bandit24.sh

mkdir /tmp/Conturo

cd /tmp/Conturo

nano banditscript.sh

​	'#!/bin/bash

​	cat /etc/bandit_pass/bandit24 >> /tmp/Conturo/bandit24'

chmod 777 banditscript.sh

cp banditscript.sh /var/spool/bandit24

chmod 777 /tmp/Conturo

ls

cat bandit24




## Conclusion
This level was quite difficult and took a lot of research to solve. The toughest part for me was figuring out how to create a script and how to execute it. For the most part, I had a general idea of what needed to be in the script due to the previous level, it was just a question of how to implement that code. The next annoying part was learning that permissions had to be given to allow the file to be run.