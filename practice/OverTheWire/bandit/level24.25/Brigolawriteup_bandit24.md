# Bandit Level 24
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 24 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | nano, nc                    |

## Summary

To solve this problem, I had to write my own shell script to brute force a PIN code to get the password from a network daemon.

## Problem
A daemon is listening on port 30002 and will give you the password for bandit25 if given the password for bandit24 and a secret numeric 4-digit pincode. There is no way to retrieve the pincode except by going through all of the 10000 combinations, called brute-forcing.

## Solution
After logging into bandit24, I immediately created and changed to a temporary directory with 'mkdir /tmp/conturo' and 'cd /tmp/conturo'. From there, I made a script with 'nano bruteforce' and wrote a simple for loop with:

`#!/bin/bash`

`for i in {0000..9999}`

`do`

​		`echo "UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ $i"`

`done | nc localhost 30002`

This code just went through every possible pin combination until the correct one was reached and gave me the password.


## Conclusion
This problem was pretty easy due to everything learned from the previous level. My prior coding experience also helped with understanding the conceptual piece of the level and what I needed to do. The only thing I had to figure out was what the syntax of a for loop in bash is.