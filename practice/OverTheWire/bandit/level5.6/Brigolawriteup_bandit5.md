# Bandit Level 5
By Massimo Brigola
___


| Problem Info | Value                      |
| ------------ | -------------------------- |
| Problem      | OverTheWire Bandit Level 5 |
| CTF          | Bandit                     |
| year         | 2020                       |
| points       | 0                          |
| category     | training, bandit           |
| tags         | find                       |


## Summary
The main thing I had to research to solve this level was how to use the find command to filter through a large amount of files. I found that I just had to scroll through the manual page to find the tests that I needed.

## Problem
The password for the next level is stored in a file somewhere under the **inhere** directory and has all of the following properties:

- human-readable
- 1033 bytes in size
- not executable

## Solution
First I had to login to level 5 by typing the command below and then typing in the password:

ssh bandit5@bandit.labs.overthewire.org -p 2220

I then used the command 'ls' to list the directories and find the directory name to switch into. After I used the command 'cd inhere' to get into the desired directory, I then read up on the manual page for the 'find' command to see what arguments I should use. I found that there was an argument that perfectly matched each property I needed. I used this command:

find -size 1033c -readable ! -executable

This command only gave me one file in return, so I just put that in a 'cat' command and got the password


## Conclusion
This was helpful for learning about the find command and how you can stack arguments with eachother.





