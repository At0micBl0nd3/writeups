Networking Foundations - Level 4: 300 points
============================================
Bailey Compton
--------------

We are given a .pcap file, which we immediately think to put into WireShark. When it opens we see this:

![wireshark capture](https://gitlab.com/d0gs/writeups/blob/559569d07b58eac88aa93bdf9320347ed8d3d97c/Maryland_Cyber_Challenge_Quals_Round2/NetFoundLevel4/wireshark.PNG)


Obviously, the packet containing the GET request for /flag.docx catches our attention. Since it is a Word document file, our immediate thought is to extract the file. 
(This is done through selecting the packet, then clicking File>Export Objects>HTTP, and saving the file)

We open the .docx file and receive the flag:

![flag](https://gitlab.com/d0gs/writeups/blob/559569d07b58eac88aa93bdf9320347ed8d3d97c/Maryland_Cyber_Challenge_Quals_Round2/NetFoundLevel4/flag.PNG)